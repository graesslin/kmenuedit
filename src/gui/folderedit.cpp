/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "folderedit.h"

using namespace KMenuEdit;

FolderEdit::FolderEdit(QWidget *parent, Qt::WindowFlags f)
    : QWidget (parent, f)
{
    setupUi(this);
}

FolderEdit::~FolderEdit()
{
}

void FolderEdit::setName(const QString &name)
{
    nameEdit->setText(name);
}

QString FolderEdit::name() const
{
    return nameEdit->text();
}

void FolderEdit::setComment(const QString &comment)
{
    commentEdit->setText(comment);
}

QString FolderEdit::comment() const
{
    return commentEdit->text();
}

QString FolderEdit::icon() const
{
    return iconButton->icon();
}

void FolderEdit::setIcon(const QString &icon)
{
    iconButton->setIcon(icon);
}

bool FolderEdit::isHidden() const
{
    return hiddenBox->isChecked();
}

void FolderEdit::setHidden(bool hidden)
{
    hiddenBox->setChecked(hidden);
}

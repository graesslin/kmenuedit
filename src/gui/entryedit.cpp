/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "entryedit.h"

using namespace KMenuEdit;

EntryEdit::EntryEdit(QWidget *parent, Qt::WindowFlags f)
    : QWidget (parent, f)
{
    setupUi(this);
}

EntryEdit::~EntryEdit()
{
}

void EntryEdit::setName(const QString &name)
{
    nameEdit->setText(name);
}

QString EntryEdit::name() const
{
    return nameEdit->text();
}

void EntryEdit::setComment(const QString &comment)
{
    commentEdit->setText(comment);
}

QString EntryEdit::comment() const
{
    return QString(commentEdit->text());
}

QString EntryEdit::icon() const
{
    return iconButton->icon();
}

void EntryEdit::setIcon(const QString &icon)
{
    iconButton->setIcon(icon);
}

QString EntryEdit::description() const
{
    return QString(descriptionEdit->text());
}

void EntryEdit::setDescription(const QString &description)
{
    descriptionEdit->setText(description);
}

QString EntryEdit::command() const
{
    return kurlrequester->lineEdit()->text();
}

void EntryEdit::setCommand (const QString &command)
{
    kurlrequester->lineEdit()->setText(command);
}

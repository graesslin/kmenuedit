/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menuitemdelegate.h"
#include "entryedit.h"
#include "folderedit.h"
#include "core/menumodel.h"
// Qt
#include <QtGui/QApplication>
#include <QtGui/QPainter>

namespace KMenuEdit
{
MenuItemDelegate::MenuItemDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
    connect(this, SIGNAL(closeEditor(QWidget*,QAbstractItemDelegate::EndEditHint)), SLOT(editFinished()));
    // create a temporary FolderEdit to get the size hint
    FolderEdit tmp;
    m_folderSizeHint = tmp.sizeHint();
    // same for EntryEdit
    EntryEdit tmp2;
    m_entrySizeHint = tmp2.sizeHint();
}

MenuItemDelegate::~MenuItemDelegate()
{
}

void MenuItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (index != m_editedIndex) {
        QStyleOptionViewItem option2(option);
        if (index.data(MenuModel::HiddenRole).toBool()) {
            option2.font.setStrikeOut(true);
        }
        QStyledItemDelegate::paint(painter, option2, index);
    } else {
        QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);
    }
    const bool separator = index.data(MenuModel::SeparatorRole).toBool();
    if (separator) {
        painter->drawLine(option.rect.left(), option.rect.center().y(), option.rect.right(), option.rect.center().y());
    }
}

QWidget *MenuItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    if (index.data(MenuModel::SeparatorRole).toBool()) {
        return NULL;
    }
    // HACK: we need to update the size hint for the currently edited item. To do that we store the index and emit the sizeHintChanged signal
    const_cast<MenuItemDelegate*>(this)->m_editedIndex = index;
    emit const_cast<MenuItemDelegate*>(this)->sizeHintChanged(index);
    if (index.data(MenuModel::FolderRole).toBool()) {
        return new FolderEdit(parent);
    } else if (index.data(MenuModel::EntryRole).toBool()) {
        EntryEdit *edit = new EntryEdit(parent);
        edit->setFocusPolicy(Qt::StrongFocus);
        return edit;
    }
    return NULL;
}

QSize MenuItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const QSize origSize = QStyledItemDelegate::sizeHint(option, index);
    if (m_editedIndex == index) {
        if (index.data(MenuModel::FolderRole).toBool()) {
            return QSize(origSize.width(), m_folderSizeHint.height());
        } else if (index.data(MenuModel::EntryRole).toBool()) {
            return QSize(origSize.width(), m_entrySizeHint.height());
        }
    }
    return origSize;
}

void MenuItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}

void MenuItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QList<QByteArray> dynamicProperties = model->dynamicPropertyNames();
    const QVariant name = editor->property("name");
    const QVariant comment = editor->property("comment");
    const QVariant icon = editor->property("icon");
    const QVariant hidden = editor->property("hidden");
    const QVariant description = editor->property("description");
    const QVariant command = editor->property("command");
    if (!dynamicProperties.contains("name")) {
        model->setData(index, name, Qt::EditRole);
    }
    if (!dynamicProperties.contains("comment")) {
        model->setData(index, comment, MenuModel::CommentRole);
    }
    if (!dynamicProperties.contains("icon")) {
        model->setData(index, icon, MenuModel::IconNameRole);
    }
    if (!dynamicProperties.contains("hidden")) {
        model->setData(index, hidden, MenuModel::HiddenRole);
    }
    if (!dynamicProperties.contains("description")) {
        model->setData(index, description, MenuModel::DescriptionRole);
    }
    if (!dynamicProperties.contains("command")) {
        model->setData(index, command, MenuModel::CommandRole);
    }
}

void MenuItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    // generic properties
    editor->setProperty("name", index.data(Qt::DisplayRole));
    editor->setProperty("comment", index.data(MenuModel::CommentRole));
    editor->setProperty("icon", index.data(MenuModel::IconNameRole));
    // folder entry specific properties
    editor->setProperty("hidden", index.data(MenuModel::HiddenRole));
    // menu entry specific properties
    editor->setProperty("description", index.data(MenuModel::DescriptionRole));
    editor->setProperty("command", index.data(MenuModel::CommandRole));
}

void MenuItemDelegate::editFinished()
{
    QModelIndex tmp = m_editedIndex;
    m_editedIndex = QModelIndex();
    emit sizeHintChanged(tmp);
}

} // namespace

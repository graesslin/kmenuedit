/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KMENUEDIT_ENTRYEEDIT_H
#define KMENUEDIT_ENTRYEDIT_H

#include <QtGui/QWidget>
#include "ui_entry.h"

namespace KMenuEdit
{

class EntryEdit : public QWidget, public Ui::EntryEdit
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString comment READ comment WRITE setComment)
    Q_PROPERTY(QString icon READ icon WRITE setIcon)
    Q_PROPERTY(QString description READ description WRITE setDescription)
    Q_PROPERTY(QString command READ command WRITE setCommand)
public:
    explicit EntryEdit(QWidget *parent = 0, Qt::WindowFlags f = 0);
    virtual ~EntryEdit();

    QString name() const;
    QString comment() const;
    QString icon() const;
    QString description() const;
    QString command() const;

    void setName(const QString &name);
    void setComment(const QString &comment);
    void setIcon(const QString &icon);
    void setDescription(const QString &description);
    void setCommand(const QString &command);
};

}

#endif // KMENUEDIT_ENTRYEDIT_H

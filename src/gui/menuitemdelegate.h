/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KMENUEDIT_MENUITEMDELEGATE_H
#define KMENUEDIT_MENUITEMDELEGATE_H

#include <QtGui/QStyledItemDelegate>

namespace KMenuEdit
{

class MenuItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit MenuItemDelegate(QObject *parent = 0);
    virtual ~MenuItemDelegate();
    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

private Q_SLOTS:
    void editFinished();

private:
    QModelIndex m_editedIndex;
    QSize m_folderSizeHint;
    QSize m_entrySizeHint;
};
} // namespace

#endif // KMENUEDIT_MENUITEMDELEGATE_H

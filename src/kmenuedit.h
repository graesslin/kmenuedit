/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KMENUEDIT_H
#define KMENUEDIT_H


#include <KXmlGuiWindow>

namespace KMenuEdit
{
class FilteredMenuModel;
}

class KMenuEditView;
class QPrinter;
class KToggleAction;
class KUrl;

/**
 * This class serves as the main window for KMenuEdit.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 */
class KMenuEditWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    KMenuEditWindow();

    /**
     * Default Destructor
     */
    virtual ~KMenuEditWindow();

protected:
    virtual bool queryClose();

private slots:
    void save();
    void restoreMenu();
    void createNewSeparator();
    void enableSave();

private:
    void setupActions();
    void sendReloadMessage();
    void setEnableSave(bool enable);

private:
    KMenuEditView *m_view;
    KMenuEdit::FilteredMenuModel *m_model;
};

#endif // _KMENUEDIT_H_

/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "kmenueditview.h"
#include "core/menumodel.h"
#include "gui/menuitemdelegate.h"

#include <KLocale>
#include <QtGui/QLabel>

KMenuEditView::KMenuEditView(QAbstractItemModel *model, QWidget *parent)
    : QTreeView(parent)
{
    setModel(model);
    setItemDelegate(new KMenuEdit::MenuItemDelegate(this));
    setHeaderHidden(true);
    setAutoFillBackground(true);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setAcceptDrops(true);
    setDragEnabled(true);
    setDropIndicatorShown(true);
}

KMenuEditView::~KMenuEditView()
{

}

#include "kmenueditview.moc"

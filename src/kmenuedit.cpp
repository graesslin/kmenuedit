/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2000 Matthias Elter <elter@kde.org>
    Copyright (C) 2001-2002 Raffaele Sandrini <sandrini@kde.org>
    Copyright (C) 2003 Waldo Bastian <bastian@kde.org>
    Copyright (C) 2008 Laurent Montel <montel@kde.org>
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "kmenuedit.h"
#include "kmenueditview.h"
#include "core/menumodel.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusMessage>
#include <QtGui/QApplication>
#include <QtGui/QDropEvent>

#include <KConfigDialog>
#include <KDE/KBuildSycocaProgressDialog>
#include <KDE/KMessageBox>
#include <KDE/KStandardDirs>
#include <KDE/KIO/NetAccess>

#include <KAction>
#include <KActionCollection>
#include <KStandardAction>

#include <KLocale>

KMenuEditWindow::KMenuEditWindow()
    : KXmlGuiWindow(),
      m_model(new KMenuEdit::FilteredMenuModel(this))
{
    m_view = new KMenuEditView(m_model, this);

    // tell the KXmlGuiWindow that this is indeed the main widget
    setCentralWidget(m_view);

    // then, setup our actions
    setupActions();

    // a call to KXmlGuiWindow::setupGUI() populates the GUI
    // with actions, using KXMLGUI.
    // It also applies the saved mainwindow settings, if any, and ask the
    // mainwindow to automatically save settings if changed: window size,
    // toolbar position, icon size, etc.
    setupGUI();

    connect(m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(enableSave()));
    connect(m_model->sourceModel(), SIGNAL(rowsInserted(QModelIndex,int,int)), SLOT(enableSave()));
    connect(m_model->sourceModel(), SIGNAL(rowsMoved(QModelIndex,int,int,QModelIndex,int)), SLOT(enableSave()));
}

KMenuEditWindow::~KMenuEditWindow()
{
}

void KMenuEditWindow::setupActions()
{
    KAction *action = KStandardAction::save(this, SLOT(save()), actionCollection());
    action->setEnabled(false);
    KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollection());

    action = new KAction(i18n("Restore to System Menu"), this);
    actionCollection()->addAction("restore_system_menu", action);
    connect( action, SIGNAL(triggered(bool)), SLOT(restoreMenu()));

    action = actionCollection()->addAction("newsep");
    action->setIcon(KIcon("menu_new_sep"));
    action->setText(i18n("New S&eparator"));
    connect(action, SIGNAL(triggered(bool)), SLOT(createNewSeparator()));

    action = new KAction(i18n("Show Hidden Entries"), this);
    action->setCheckable(true);
    actionCollection()->addAction("show_hidden_entries", action);
    connect(action, SIGNAL(triggered(bool)), m_model, SLOT(showHiddenEntries(bool)));
}

void KMenuEditWindow::sendReloadMessage()
{
    QDBusMessage message = QDBusMessage::createSignal("/kickoff", "org.kde.plasma", "reloadMenu");
    QDBusConnection::sessionBus().send(message);
}

void KMenuEditWindow::save()
{
    m_model->save();
    KBuildSycocaProgressDialog::rebuildKSycoca(this);
    sendReloadMessage();
    setEnableSave(false);
}

void KMenuEditWindow::restoreMenu()
{
    // based on old implementation before rewrite - see Copyright header
    if (KMessageBox::warningYesNo(this, i18nc("Warning shown in a dialog message before restoring to system menu",
        "Do you want to restore the system menu? Warning: This will remove all custom menus.")) == KMessageBox::No) {
        return;
    }
    const QString kmenueditfile = KStandardDirs::locateLocal("xdgconf-menu", "applications-kmenuedit.menu");
    if (QFile::exists(kmenueditfile)) {
        if (!QFile::remove(kmenueditfile )) {
            qWarning() << "Could not delete " << kmenueditfile;
        }
    }

    QString xdgdir = KGlobal::dirs()->KStandardDirs::localxdgdatadir();
    if (!KIO::NetAccess::del(QString(xdgdir + "/applications") , this)) {
        qWarning() << "Could not delete dir :" << (xdgdir + "/applications");
    }
    if (!KIO::NetAccess::del(QString(xdgdir + "/desktop-directories") , this)) {
        qWarning() << "Could not delete dir :" << ( xdgdir + "/desktop-directories");
    }

    // reload the menu
    KBuildSycocaProgressDialog::rebuildKSycoca(this);
    m_model->reload();
    sendReloadMessage();
    setEnableSave(false);
}

void KMenuEditWindow::createNewSeparator()
{
    m_model->insertNewSeparator(m_view->currentIndex());
}

void KMenuEditWindow::enableSave()
{
    setEnableSave(true);
}

void KMenuEditWindow::setEnableSave(bool enable)
{
    actionCollection()->action(KStandardAction::name(KStandardAction::Save))->setEnabled(enable);
}

bool KMenuEditWindow::queryClose()
{
    if (!m_model->isChanged()) {
        return true;
    }

    const int result = KMessageBox::warningYesNoCancel(this,
                                             i18n("You have made changes to the menu.\n"
                         "Do you want to save the changes or discard them?"),
                                             i18n("Save Menu Changes?"),
                                             KStandardGuiItem::save(), KStandardGuiItem::discard() );

    if (result == KMessageBox::Yes) {
        // quit with saving
        save();
        return true;
    } else if (result == KMessageBox::No) {
        // quit without saving
        return true;
    }
    // cancel
    return false;
}

#include "kmenuedit.moc"

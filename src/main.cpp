/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "kmenuedit.h"
#include <KDE/KApplication>
#include <KDE/KAboutData>
#include <KDE/KCmdLineArgs>
#include <KDE/KLocale>

static const char description[] = I18N_NOOP("KDE menu editor");
static const char version[] = "2.0";

int main(int argc, char **argv)
{
    KAboutData aboutData("kmenuedit", 0, ki18n("KDE Menu Editor"),
                         version, ki18n(description), KAboutData::License_GPL,
                         ki18n("(C) 2000-2012, Martin Gräßlin, Waldo Bastian, Raffaele Sandrini, Matthias Elter"));
    aboutData.addAuthor(ki18n("Martin Gräßlin"), ki18n("Maintainer"), "mgraesslin@kde.org");
    aboutData.addAuthor(ki18n("Waldo Bastian"), ki18n("Previous Maintainer"), "bastian@kde.org");
    aboutData.addAuthor(ki18n("Raffaele Sandrini"), ki18n("Previous Maintainer"), "sandrini@kde.org");
    aboutData.addAuthor(ki18n("Matthias Elter"), ki18n("Original Author"), "elter@kde.org");
    aboutData.addAuthor(ki18n("Montel Laurent"), KLocalizedString(), "montel@kde.org");
    aboutData.addCredit(ki18nc("Project which sponsored the development of version 2", "Netrunner"),
                        ki18n("For Sponsoring the development of Version 2.0"),
                        QByteArray(), "http://www.netrunner-os.com/");
    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions options;
    KCmdLineArgs::addCmdLineOptions(options);
    KApplication app;

    KMenuEditWindow *widget = new KMenuEditWindow;

    // see if we are starting with session management
    if (app.isSessionRestored())
    {
        RESTORE(KMenuEditWindow);
    } else {
        // no session.. just start up normally
        KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
        if (args->count() == 0)
        {
            //kmenuedit *widget = new kmenuedit;
            widget->show();
        }
        else
        {
            int i = 0;
            for (; i < args->count(); i++)
            {
                //kmenuedit *widget = new kmenuedit;
                widget->show();
            }
        }
        args->clear();
    }

    return app.exec();
}

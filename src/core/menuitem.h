/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KMENUEDIT_MENUITEM_H
#define KMENUEDIT_MENUITEM_H
// KDE
#include <KDE/KServiceGroup>
// Qt
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtGui/QIcon>

namespace KMenuEdit
{
class MenuFolder;

/**
 * @short Abstract base class for all parts of a menu.
 *
 * To construct a MenuItem use one of the static factory methods.
 **/
class MenuItem
{
public:
    virtual ~MenuItem();

    const MenuFolder *parent() const {
        return m_parent;
    }

    MenuFolder *parent() {
        return m_parent;
    }

    const QString &name() const {
        return m_name;
    }

    const QIcon &icon() const {
        return m_icon;
    }

    const QString &comment() const {
        return m_comment;
    }

    const QString &iconName() const {
        return m_iconName;
    }

    const QString &genericName() const {
        return m_genericName;
    }

    bool isChanged() const {
        return m_changed;
    }

    void setName(const QString &name);
    void setIconName(const QString &iconName);
    void setComment(const QString &comment);
    void setGenericName(const QString &genericName);

    void setAdded(bool added) {
        m_added = added;
    }
    bool isAdded() const {
        return m_added;
    }
    void setRemoved(bool removed) {
        m_removed = removed;
    }
    bool isRemoved() const {
        return m_removed;
    }

    virtual bool isSeparator() const {
        return false;
    }

    bool isHidden() const {
        return m_hidden;
    }
    void setHidden(bool hidden);
    virtual bool isFolder() const {
        return false;
    }
    /**
     * Saves the entry.
     * Default implementation does nothing.
     **/
    virtual void save() {}
    virtual QString menuId() const {
        return QString();
    };

    /**
     * Factory method for a MenuFolder.
     **/
    static MenuItem *folder(KServiceGroup::Ptr group, MenuFolder *parent = NULL);
    /**
     * Factory method for a MenuEntry.
     **/
    static MenuItem *entry(KService::Ptr service, MenuFolder *parent = NULL);
    /**
     * Factory method for a separator item.
     **/
    static MenuItem *separator(KSycocaEntry::Ptr separator, MenuFolder *parent = NULL);
    static MenuItem *createCopy(MenuItem const *item, MenuFolder *parent = NULL);

protected:
    /**
     * Ctor used for a MenuFolder.
     **/
    MenuItem(KServiceGroup::Ptr group, MenuFolder *parent = NULL);
    /**
     * Ctor used for a MenuEntry.
     **/
    MenuItem(KService::Ptr service, MenuFolder *parent = NULL);
    /**
     * Ctor used for a MenuSeparator.
     **/
    MenuItem(KSycocaEntry::Ptr separator, MenuFolder *parent);
    /**
     * @returns the underlying service to be used by an inheriting class.
     **/
    KSycocaEntry::Ptr service() const {
        return m_service;
    };
    void setChanged(bool changed) {
        m_changed = changed;
    }
    void setParent(MenuFolder *newParent) {
        m_parent = newParent;
    }

private:
    /**
     * Pointer to the parent item. Is null for the root item.
     **/
    MenuFolder *m_parent;
    /**
     * The actual represented data.
     **/
    KSycocaEntry::Ptr m_service;
    QString m_name;
    QString m_genericName;
    QString m_iconName;
    QIcon m_icon;
    QString m_comment;
    bool m_hidden;
    bool m_changed;
    bool m_added;
    bool m_removed;
};

/**
 * @short MenuItem representing a folder containing additional MenuItems.
 **/
class MenuFolder : public MenuItem
{
public:
    virtual ~MenuFolder();

    /**
     * @returns number of child MenuItems in this folder.
     **/
    int childCount() const;
    /**
     * @returns The child MenuItem at @p index.
     **/
    MenuItem* child(int index) const;
    /**
     * @returns the index of the given child @p item.
     **/
    int indexOf(MenuItem* item) const;
    virtual bool isFolder() const {
        return true;
    }
    virtual void save();
    virtual QString menuId() const;
    /**
     * Finds the child folder with the given @p menuId.
     * @param menuId The menuId of the folder to find
     * @return Pointer to the folder or @c null if not found.
     **/
    const MenuFolder *findFolder(const QString& menuId) const;
    /**
     * Finds the child menu entry with the given @p menuId.
     * This method only looks through the direct children.
     * @param menuId The child menu entry with the given id.
     * @return Pointer to the menu entry or @c null if not found.
     **/
    const MenuItem *findItem(const QString& menuId) const;
    const MenuItem *findItem(int menuId) const;
    void insertItem(MenuItem const *item, int index = -1);
    void insertNewSeparator(int index = -1);
    void moveItem(int sourceIndex, int destinationIndex);
    void moveToFolder(MenuFolder *target, int targetIndex);
    void setLayoutDirty(bool dirty);
    bool isLayoutDirty() const {
        return m_layoutDirty;
    }
    /**
     * @returns @c true if any item in the tree has a changed layout.
     **/
    bool isTreeLayoutDirty() const;
    /**
     *
     * @returns @c true if any item in the tree is changed.
     * @see isChanged
     **/
    bool isTreeChanged() const;
    /**
     * @returns Layout of this folder without recursion
     **/
    QStringList extractLayout() const;
    /**
     * @returns whether the menu folder has been moved
     **/
    bool isMoved() const {
        return m_moved;
    }
    void setMoved(bool moved) {
        m_moved = moved;
    }

private:
    MenuFolder(KServiceGroup::Ptr group, MenuFolder *parent = NULL);
    /**
     * Build up this folder recursively by adding all children to the internal list.
     **/
    void init();
    /**
     * List of children this folder contains. Single entries could be further folders containing
     * additional child items.
     **/
    QList<MenuItem*> m_entries;
    friend class MenuItem;
    bool m_layoutDirty;
    bool m_moved;
    QString m_originalMenuId;
};

/**
 * @short A MenuItem representing an actual item which is not a folder.
 **/
class MenuEntry : public MenuItem
{
public:
    virtual ~MenuEntry();

    KDesktopFile *desktopFile() const;
    virtual void save();

    virtual QString menuId() const;

private:
    MenuEntry(KService::Ptr group, MenuFolder *parent = NULL);
    friend class MenuItem;
    KDesktopFile *m_desktopFile;
};

/**
 * @short A MenuItem representing a separator. This type does not hold any data.
 **/
class MenuSeparator : public MenuItem
{
public:
    virtual ~MenuSeparator();

    virtual bool isSeparator() const {
        return true;
    }
private:
    MenuSeparator(KSycocaEntry::Ptr separator, MenuFolder *parent = NULL);
    friend class MenuItem;
};

} // namespace

#endif // KMENUEDIT_MENUITEM_H

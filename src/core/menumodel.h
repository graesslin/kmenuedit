/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KMENUEDIT_MENUMODEL_H
#define KMENUEDIT_MENUMODEL_H

#include <QtGui/QSortFilterProxyModel>

namespace KMenuEdit
{
class MenuItem;
class MenuFile;
class MenuFolder;

class MenuModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum MenuItemRoles {
        ///< Item is a Separator
        SeparatorRole = Qt::UserRole,
        ///< Item is a Folder
        FolderRole,
        ///< Item is a normal Menu Entry
        EntryRole,
        ///< The Comment
        CommentRole,
        ///< The name of the icon
        IconNameRole,
        ///< Whether the folder is hidden
        HiddenRole,
        ///< The description of a Menu Entry
        DescriptionRole,
        ///< The command of a Menu Entry
        CommandRole,
        ///< The MenuId
        MenuIdRole
    };
    virtual ~MenuModel();

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    virtual QStringList mimeTypes() const;
    virtual QMimeData* mimeData(const QModelIndexList &indexes) const;
    virtual Qt::DropActions supportedDropActions() const;

    /**
     * Creates a separator and inserts it as a child to @p index in case @p index points to
     * a folder or after @p index, if it is not a folder.
     * @param index The ModelIndex where to insert the new separator.
     **/
    void insertNewSeparator(const QModelIndex &index);

    bool isChanged() const;

public Q_SLOTS:
    /**
     * Saves the complete Model.
     **/
    void save();
    /**
     * Resets the complete Model and reloads from files.
     **/
    void reload();

private:
    /**
     * Private ctor. Create a FilteredMenuModel instead which will create an instance of this Model.
     **/
    explicit MenuModel(QObject *parent = 0);
    void init();
    QModelIndex indexOf(MenuItem* item) const;
    void saveLayout(const QModelIndex &parent);
    bool moveFolder(const QModelIndex &sourceIndex, const QModelIndex &targetParent, int row);
    MenuFolder *m_rootFolder;
    MenuFile *m_menuFile;

    // friend the FilteredMenuModel to allow it calling the private ctor.
    friend class FilteredMenuModel;
};

/**
 * @short Filter Model around MenuModel to include/exclude hidden entries.
 *
 * The purpose of this filter model is to filter out all hidden menu entries
 * from the view. Unless the global option is set to also show hidden entries.
 *
 * This FilterMenuModel holds the actual MenuModel and creates it in it's ctor.
 **/
class FilteredMenuModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FilteredMenuModel(QObject *parent = 0);
    void insertNewSeparator(const QModelIndex &index);

    bool isChanged() const;

public Q_SLOTS:
    void save();
    void reload();
    void showHiddenEntries(bool show);

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;

private:
    /**
     * Hiding from public interface
     **/
    virtual void setSourceModel(QAbstractItemModel* sourceModel);
    MenuModel *menu() {
        return static_cast<MenuModel*>(sourceModel());
    }
    const MenuModel *menu() const {
        return static_cast<const MenuModel*>(sourceModel());
    }
    bool m_showHidden;
};

} // namespace

#endif // KMENUEDIT_MENUMODEL_H

/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menuitem.h"

#include <KDE/KConfigGroup>
#include <KDE/KDesktopFile>
#include <KDE/KIcon>
#include <KDE/KStandardDirs>

namespace KMenuEdit
{

MenuItem *MenuItem::entry(KService::Ptr service, MenuFolder *parent)
{
    return new MenuEntry(service, parent);
}

MenuItem *MenuItem::folder(KServiceGroup::Ptr group, MenuFolder *parent)
{
    return new MenuFolder(group, parent);
}

MenuItem *MenuItem::separator(KSycocaEntry::Ptr separator, KMenuEdit::MenuFolder *parent)
{
    return new MenuSeparator(separator, parent);
}

MenuItem* MenuItem::createCopy(const MenuItem* item, MenuFolder* parent)
{
    // TODO: is this actually correct? Do we have to create new entries?
    if (const MenuEntry *entry = dynamic_cast<const MenuEntry *>(item)) {
        // TODO: handle dirty case
        return MenuItem::entry(KService::Ptr::staticCast(entry->service()), parent);
    }
    if (const MenuSeparator *separator = dynamic_cast<const MenuSeparator *>(item)) {
        // TODO: handle dirty case
        return MenuItem::separator(KSycocaEntry::Ptr::staticCast(separator->service()), parent);
    }
    if (const MenuFolder *folder = dynamic_cast<const MenuFolder *>(item)) {
        // TODO: handle dirty case
        return MenuItem::folder(KServiceGroup::Ptr::staticCast(folder->service()), parent);
    }
    return NULL;
}

MenuItem::MenuItem(KSycocaEntry::Ptr separator, MenuFolder *parent)
    : m_parent(parent)
    , m_service(separator)
    , m_name(QString())
    , m_genericName(QString())
    , m_iconName(QString())
    , m_icon(QIcon())
    , m_comment(QString())
    , m_hidden(false)
    , m_changed(false)
    , m_added(false)
    , m_removed(false)
{
}

MenuItem::MenuItem (KServiceGroup::Ptr group, MenuFolder *parent)
    : m_parent(parent)
    , m_service(KSycocaEntry::Ptr::staticCast(group))
    , m_name(group->caption())
    , m_genericName(QString())
    , m_iconName(group->icon())
    , m_icon(KIcon(group->icon()))
    , m_comment(group->comment())
    , m_hidden(group->noDisplay())
    , m_changed(false)
    , m_added(false)
    , m_removed(false)
{
}

MenuItem::MenuItem(KService::Ptr service, MenuFolder *parent)
    : m_parent(parent)
    , m_service(KSycocaEntry::Ptr::staticCast(service))
    , m_name(service->name())
    , m_genericName(service->genericName())
    , m_iconName(service->icon())
    , m_icon(KIcon(service->icon()))
    , m_comment(service->comment())
    , m_hidden(service->noDisplay())
    , m_changed(false)
    , m_added(false)
    , m_removed(false)
{
}

MenuItem::~MenuItem()
{
}

#define SETTER( type, name, variable) \
void MenuItem::name( type val ) { \
    if (variable == val) { \
        return; \
    } \
    variable = val; \
    m_changed = true; \
}
SETTER(const QString&, setName, m_name)
SETTER(const QString&, setGenericName, m_genericName)
SETTER(const QString&, setComment, m_comment)
#undef SETTER

void MenuItem::setIconName(const QString &iconName)
{
    if (m_iconName == iconName) {
        return;
    }
    m_iconName = iconName;
    m_icon = KIcon(m_iconName);
    m_changed = true;
}

void MenuItem::setHidden(bool hidden)
{
    if (hidden == m_hidden) {
        return;
    }
    m_hidden = hidden;
    m_changed = true;
    if (parent()) {
        parent()->setLayoutDirty(true);
    }
}

MenuFolder::MenuFolder(KServiceGroup::Ptr group, KMenuEdit::MenuFolder *parent)
    : MenuItem(group, parent)
    , m_layoutDirty(false)
    , m_moved(false)
{
    init();
}

MenuFolder::~MenuFolder()
{
    qDeleteAll(m_entries);
    m_entries.clear();
}

void MenuFolder::init()
{
    KServiceGroup::Ptr group(KServiceGroup::Ptr::staticCast(service()));
    const KServiceGroup::List children = group->entries(true, false, true);
    foreach (const KSycocaEntry::Ptr &e, children) {
        if (e->isType(KST_KServiceGroup)) {
            m_entries.append(MenuItem::folder(KServiceGroup::Ptr::staticCast(e), this));
        }
        else if (e->isType(KST_KService)) {
            m_entries.append(MenuItem::entry(KService::Ptr::staticCast(e), this));
        } else if (e->isType(KST_KServiceSeparator)) {
            m_entries.append(MenuItem::separator(e, this));
        }
    }
}

int MenuFolder::childCount() const
{
    return m_entries.count();
}

MenuItem *MenuFolder::child(int index) const
{
    if (index >= m_entries.count()) {
        return NULL;
    }
    return m_entries.at(index);
}

int MenuFolder::indexOf(MenuItem *item) const
{
    return m_entries.indexOf(item);
}

void MenuFolder::save()
{
    if (isChanged() || isMoved()) {
        const QString directoryFile = KServiceGroup::Ptr::staticCast(service())->directoryEntryPath();
        QString local = KDesktopFile::locateLocal(directoryFile);

        KDesktopFile *df = 0;
        if (directoryFile != local) {
            KDesktopFile orig("apps", directoryFile);
            df = orig.copyTo(local);
        } else {
            df = new KDesktopFile("apps", directoryFile);
        }

        df->desktopGroup().writeEntry("Name", name());
        df->desktopGroup().writeEntry("Comment", comment());
        df->desktopGroup().writeEntry("Icon", iconName());
        df->desktopGroup().sync();
        delete df;
    }
    // now save recursively all other items
    foreach (MenuItem *item, m_entries) {
        item->save();
    }
    setChanged(false);
}

QString MenuFolder::menuId() const
{
    return service()->name();
}

const MenuFolder* MenuFolder::findFolder(const QString& menuId) const
{
    if (this->menuId() == menuId) {
        return this;
    }
    foreach (MenuItem *item, m_entries) {
        if (!item->isFolder()) {
            continue;
        }
        MenuFolder *folder = static_cast<MenuFolder*>(item);
        if (MenuFolder const *child = folder->findFolder(menuId)) {
            return child;
        }
    }
    return NULL;
}

const MenuItem* MenuFolder::findItem(const QString& menuId) const
{
    foreach (MenuItem const *item, m_entries) {
        if (item->isSeparator()) {
            continue;
        }
        if (item->menuId() == menuId) {
            return item;
        }
    }
    return NULL;
}

const MenuItem* MenuFolder::findItem(int menuId) const
{
    if (menuId < 0 || menuId >= m_entries.size()) {
        return NULL;
    }
    return m_entries.at(menuId);
}

void MenuFolder::insertItem(const MenuItem* item, int index)
{
    MenuItem *newItem = MenuItem::createCopy(item, this);
    if (!newItem) {
        return;
    }
    newItem->setAdded(true);
    if (index == -1) {
        m_entries.append(newItem);
    } else {
        m_entries.insert(index, newItem);
    }
    setLayoutDirty(true);
}

void MenuFolder::insertNewSeparator(int index)
{
    MenuItem *newSeparator = MenuItem::separator(KSycocaEntry::Ptr(new KSycocaEntry()), this);
    if (index == -1) {
        m_entries.append(newSeparator);
    } else {
        m_entries.insert(index, newSeparator);
    }
    setLayoutDirty(true);
}

void MenuFolder::moveItem(int sourceIndex, int destinationIndex)
{
    if (destinationIndex >= m_entries.size()) {
        destinationIndex = m_entries.size() - 1;
    }
    m_entries.move(sourceIndex, destinationIndex);
    setLayoutDirty(true);
}

void MenuFolder::moveToFolder(MenuFolder *target, int targetIndex)
{
    if (parent()) {
        parent()->m_entries.removeAll(this);
    }
    target->m_entries.insert(targetIndex, this);
    target->setLayoutDirty(true);
    m_moved = true;
    setParent(target);
}

bool MenuFolder::isTreeLayoutDirty() const
{
    if (isLayoutDirty()) {
        return true;
    }
    foreach (MenuItem *entry, m_entries) {
        if (entry->isFolder()) {
            if (dynamic_cast<MenuFolder*>(entry)->isTreeLayoutDirty()) {
                return true;
            }
        }
    }
    return false;
}

void MenuFolder::setLayoutDirty(bool dirty)
{
    m_layoutDirty = dirty;
}

bool MenuFolder::isTreeChanged() const
{
    if (isChanged()) {
        return true;
    }
    foreach (MenuItem *item, m_entries) {
        // loop over all items and check whether they are changed
        if (item->isChanged()) {
            return true;
        }
    }
    foreach (MenuItem *item, m_entries) {
        // and go over all folders and recursively find a changed subtree
        if (item->isFolder() && static_cast<MenuFolder*>(item)->isTreeChanged()) {
            return true;
        }
    }
    return false;
}

QStringList MenuFolder::extractLayout() const
{
    QStringList layout;
    bool firstFolder = true;
    bool firstEntry = true;
    foreach (MenuItem *item, m_entries) {
        if (item->isFolder()) {
            if (firstFolder) {
                firstFolder = false;
                layout << ":M"; // Add new folders here...
            }
            layout << static_cast<MenuFolder*>(item)->menuId();
        } else if (item->isSeparator()) {
            layout << ":S";
        } else {
            if (firstEntry) {
                firstEntry = false;
                layout << ":F"; // Add new entries here...
            }
            layout << static_cast<MenuEntry*>(item)->menuId();
        }
    }
    return layout;
}

MenuEntry::MenuEntry(KService::Ptr service, MenuFolder *parent)
    : MenuItem(service, parent)
    , m_desktopFile(new KDesktopFile(service->entryPath()))
{
}

MenuEntry::~MenuEntry()
{
    delete m_desktopFile;
}

KDesktopFile *MenuEntry::desktopFile() const
{
    return m_desktopFile;
}

void MenuEntry::save()
{
    if (!isChanged()) {
        // not changed, nothing to save.
        return;
    }
    KService::Ptr entryService = KService::Ptr::staticCast(service());
    const QString local = KStandardDirs::locateLocal("xdgdata-apps", entryService->menuId());
    if (local != entryService->entryPath()) {
        // copy the desktop file to user's directory if it is not already there
        KDesktopFile *oldDf = m_desktopFile;
        m_desktopFile = oldDf->copyTo(local);
        delete oldDf;
    }
    m_desktopFile->desktopGroup().writeEntry("Name", name());
    m_desktopFile->desktopGroup().writeEntry("GenericName", genericName());
    m_desktopFile->desktopGroup().writeEntry("Icon", iconName());
    m_desktopFile->sync();
    setChanged(false);
}

QString MenuEntry::menuId() const
{
    KService::Ptr entryService = KService::Ptr::staticCast(service());
    return entryService->menuId();
}

MenuSeparator::MenuSeparator(KSycocaEntry::Ptr separator, MenuFolder *parent)
    : MenuItem(separator, parent)
{
}

MenuSeparator::~MenuSeparator()
{
}

} // namespace

/*
    KMenuEdit: a menu editor for the KDE Plasma Workspaces
    Copyright (C) 2012  Martin Gräßlin <mgraesslin@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menumodel.h"
#include "menufile.h"
#include "menuitem.h"

// KDE
#include <KDE/KConfigGroup>
#include <KDE/KDesktopFile>
#include <KDE/KStandardDirs>
// Qt
#include <QtCore/QMimeData>
#include <QtCore/QDebug>

namespace KMenuEdit
{

FilteredMenuModel::FilteredMenuModel(QObject* parent)
    : QSortFilterProxyModel(parent)
    , m_showHidden(false)
{
    setSourceModel(new MenuModel(this));
    connect(sourceModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(invalidate()));
}

void FilteredMenuModel::reload()
{
    menu()->reload();
}

void FilteredMenuModel::save()
{
    menu()->save();
}

void FilteredMenuModel::insertNewSeparator(const QModelIndex& index)
{
    menu()->insertNewSeparator(mapToSource(index));
}

void FilteredMenuModel::setSourceModel(QAbstractItemModel* sourceModel)
{
    QSortFilterProxyModel::setSourceModel(sourceModel);
}

bool FilteredMenuModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    const QModelIndex index = menu()->index(source_row, 0, source_parent);
    if (index.data(MenuModel::HiddenRole).toBool()) {
        // hidden entries depend on the global setting
        return m_showHidden;
    } else {
        // non hidden entries are always accepted
        return true;
    }
}

void FilteredMenuModel::showHiddenEntries(bool show)
{
    if (m_showHidden == show) {
        return;
    }
    m_showHidden = show;
    invalidateFilter();
}

bool FilteredMenuModel::isChanged() const
{
    return menu()->isChanged();
}

MenuModel::MenuModel(QObject *parent)
    : QAbstractItemModel (parent)
    , m_rootFolder(NULL)
    , m_menuFile(new MenuFile(KStandardDirs::locateLocal("xdgconf-menu", "applications-kmenuedit.menu")))
{
    init();
}

MenuModel::~MenuModel()
{
    delete m_rootFolder;
}

void MenuModel::init()
{
    beginResetModel();
    m_rootFolder = dynamic_cast<MenuFolder*>(MenuItem::folder(KServiceGroup::root()));
    endResetModel();
}

void MenuModel::reload()
{
    beginResetModel();
    delete m_rootFolder;
    m_rootFolder = dynamic_cast<MenuFolder*>(MenuItem::folder(KServiceGroup::root()));
    endResetModel();
}

QVariant MenuModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    if (static_cast<MenuItem*>(index.internalPointer())->isSeparator()) {
        if (role == SeparatorRole) {
            return true;
        } else if (role == MenuIdRole) {
            QModelIndex parentIndex = parent(index);
            QString modelId('/');
            if (parentIndex.isValid()) {
                modelId.append(parentIndex.data(MenuIdRole).toString());
            }
            modelId.append(QString::number(index.row()));
            return modelId;
        }
        return QVariant();
    }
    switch (role) {
    case Qt::DisplayRole:
        return static_cast<MenuItem*>(index.internalPointer())->name();
    case Qt::DecorationRole:
        return static_cast<MenuItem*>(index.internalPointer())->icon();
    case CommentRole:
        return static_cast<MenuItem*>(index.internalPointer())->comment();
    case IconNameRole:
        return static_cast<MenuItem*>(index.internalPointer())->iconName();
    case DescriptionRole:
        return static_cast<MenuItem*>(index.internalPointer())->genericName();
    case HiddenRole:
        return static_cast<MenuItem*>(index.internalPointer())->isHidden();
    case SeparatorRole:
        return false;
    case FolderRole:
        return static_cast<MenuItem*>(index.internalPointer())->isFolder();
    case EntryRole:
        return !static_cast<MenuItem*>(index.internalPointer())->isFolder() && !static_cast<MenuItem*>(index.internalPointer())->isSeparator();
    case CommandRole:
        if (!static_cast<MenuItem*>(index.internalPointer())->isFolder() && !static_cast<MenuItem*>(index.internalPointer())->isSeparator()) {
            const QString command = static_cast<MenuEntry*>(index.internalPointer())->desktopFile()->desktopGroup().readEntry("Exec");
            if (command.startsWith("ksystraycmd")) {
                return command.right(command.length()-12);
            } else {
                return command;
            }
        } else {
            return QVariant();
        }
    case MenuIdRole:
        if (static_cast<MenuItem*>(index.internalPointer())->isFolder()) {
            return static_cast<MenuItem*>(index.internalPointer())->menuId();
        } else {
            QModelIndex parentIndex = parent(index);
            QString modelId('/');
            if (parentIndex.isValid()) {
                modelId.append(parentIndex.data(MenuIdRole).toString());
            }
            modelId.append(static_cast<MenuItem*>(index.internalPointer())->menuId());
            return modelId;
        }
    }
    return QVariant();
}

int MenuModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

int MenuModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return m_rootFolder->childCount();
    }
    const MenuItem *item = static_cast<MenuItem*>(parent.internalPointer());
    if (const MenuFolder *folder = dynamic_cast<const MenuFolder*>(item)) {
        return folder->childCount();
    }
    return 0;
}

QModelIndex MenuModel::parent(const QModelIndex &child) const
{
    if (!child.isValid() || child.internalPointer() == m_rootFolder) {
        // the root folder is not part of our model
        return QModelIndex();
    }
    MenuItem *item = static_cast<MenuItem*>(child.internalPointer());
    if (item->parent() == m_rootFolder) {
        // all items with root folder as parent are our top level items
        return QModelIndex();
    }
    const int row = item->parent()->parent()->indexOf(item->parent());
    if (row == -1) {
        // check for incorrect row access
        return QModelIndex();
    }
    return createIndex(row, 0, item->parent());
}

QModelIndex MenuModel::index(int row, int column, const QModelIndex &parent) const
{
    if (column != 0) {
        // we have only one column
        return QModelIndex();
    }
    if (!parent.isValid()) {
        if (row >= m_rootFolder->childCount()) {
            return QModelIndex();
        }
        // all children of the root folder are toplevel items
        return createIndex(row, column, m_rootFolder->child(row));
    } else {
        MenuFolder *folder = static_cast<MenuFolder*>(parent.internalPointer());
        if (row >= folder->childCount()) {
            // incorrect row data
            return QModelIndex();
        }
        return createIndex(row, column, folder->child(row));
    }
}

Qt::ItemFlags MenuModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        // root is a drop target
        return QAbstractItemModel::flags(index) | Qt::ItemIsDropEnabled;
    }
    if (index.data(SeparatorRole).toBool()) {
        // separators can be dragged
        return QAbstractItemModel::flags(index) | Qt::ItemIsDragEnabled;
    }
    if (index.data(FolderRole).toBool()) {
        // folders are drop targets and can be dragged
        return QAbstractItemModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled;
    }
    // items are dragable
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsDragEnabled;
}

bool MenuModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || !value.isValid()) {
        return false;
    }
    MenuItem *item = static_cast<MenuItem*>(index.internalPointer());
    if (item->isSeparator()) {
        return false;
    }
    switch (role) {
    case Qt::EditRole:
        if (!value.canConvert<QString>()) {
            return false;
        }
        item->setName(value.toString());
        break;
    case CommentRole:
        if (!value.canConvert<QString>()) {
            return false;
        }
        item->setComment(value.toString());
        break;
    case IconNameRole:
        if (!value.canConvert<QString>()) {
            return false;
        }
        item->setIconName(value.toString());
        break;
    case DescriptionRole:
        if (!value.canConvert<QString>()) {
            return false;
        }
        item->setGenericName(value.toString());
        break;
    case HiddenRole:
        if (!value.canConvert<bool>()) {
            return false;
        }
        item->setHidden(value.toBool());
        break;
    case CommandRole:
        if (!value.canConvert<QString>()) {
            return false;
        }
//         item->setCommand(value.toString());
        break;
    default:
        return false;
    }
    emit dataChanged(index, index);
    return true;
}

void MenuModel::saveLayout(const QModelIndex& parent)
{
    MenuFolder *folder = NULL;
    if (parent.isValid() && parent.data(FolderRole).toBool()) {
        folder = static_cast<MenuFolder*>(parent.internalPointer());
    } else {
        folder = m_rootFolder;
    }
    if (!folder) {
        return;
    }
    const QString menuName = parent.isValid() ? parent.data(MenuIdRole).toString() : "";
    const bool layoutDirty = folder->isLayoutDirty();
    if (layoutDirty) {
        m_menuFile->setLayout(menuName, folder->extractLayout());
        folder->setLayoutDirty(false);
    }
    for (int i=0; i<rowCount(parent); ++i) {
        QModelIndex childIndex = index(i, 0, parent);
        if (childIndex.data(FolderRole).toBool()) {
            saveLayout(childIndex);
        }
        if (layoutDirty) {
            // TODO: add entries
            MenuItem *item = static_cast<MenuItem*>(childIndex.internalPointer());
            if (item->isRemoved()) {
                if (childIndex.data(EntryRole).toBool()) {
                    // add hidden entries to exclude section
                    m_menuFile->removeEntry(menuName, item->menuId());
                    item->setRemoved(false);
                }
                // TODO: remove folders
            } else if (item->isAdded()) {
                if (childIndex.data(EntryRole).toBool()) {
                    // add new entries to include section
                    m_menuFile->addEntry(menuName, item->menuId());
                    item->setAdded(false);
                }
                // TODO: add folders
            }
            if (childIndex.data(FolderRole).toBool()) {
                MenuFolder *folderItem = static_cast<MenuFolder*>(childIndex.internalPointer());
                if (folderItem->isMoved()) {
                    QStringList parts = folderItem->menuId().left(folderItem->menuId().length() - 1).split('/');
                    m_menuFile->moveMenu(folderItem->menuId(), parent.data(MenuIdRole).toString() + parts.last() + "/");
                    folderItem->setMoved(false);
                }
            }
        }
    }
}

void MenuModel::save()
{
    m_rootFolder->save();
    if (m_rootFolder->isTreeLayoutDirty()) {
        saveLayout(QModelIndex());
        m_menuFile->save();
    }
}

QMimeData* MenuModel::mimeData(const QModelIndexList& indexes) const
{
    if (indexes.isEmpty()) {
        return NULL;
    }
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    int count = 0;
    foreach (const QModelIndex &index, indexes) {
        if (index.isValid()) {
            if (count > 0) {
                encodedData.append('\n');
            }
            if (index.data(SeparatorRole).toBool()) {
                encodedData.append("SEPARATOR");
            }
            if (index.data(FolderRole).toBool()) {
                encodedData.append("FOLDER/");
            }
            encodedData.append(index.data(MenuIdRole).toString());
            ++count;
        }
    }
    qDebug() << "Mime Data: " << QString(encodedData);

    mimeData->setData("application/x-menuitem", encodedData);
    return mimeData;
}

QStringList MenuModel::mimeTypes() const
{
    return QStringList() << "application/x-menuitem";
}

bool MenuModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (column > 0) {
        return false;
    }
    if (!data->hasFormat("application/x-menuitem")) {
        return false;
    }
    if (action == Qt::IgnoreAction) {
        return true;
    }
    if (action == Qt::LinkAction || action == Qt::TargetMoveAction) {
        return false;
    }
    // find the element to move
    QString path = data->data("application/x-menuitem");
    bool separator = false;
    bool folderItem = false;
    if (path.startsWith("SEPARATOR")) {
        separator = true;
        // remove starting separator
        path = path.right(path.length() - 9);
    }
    if (path.startsWith("FOLDER")) {
        if (action == Qt::CopyAction) {
            qDebug() << "Folders cannot be copied, only moved";
            return false;
        }
        folderItem = true;
        // remove starting folder
        path = path.right(path.length() - 6);
        // remove trailing '/'
        path = path.left(path.length() - 1);
    }
    // remove leading "/"
    path = path.right(path.length() - 1);
    int index = path.lastIndexOf('/');
    QString folderPath;
    QString itemId;
    if (index != -1) {
        // need to include the trailing "/"
        folderPath = path.left(index + 1);
        itemId = path.mid(index + 1);
    } else {
        // root folder is just the "/"
        folderPath = '/';
        itemId = path;
    }
    if (folderItem) {
        // for a folder the path is the menuId plus the trailing slash we cut off before
        itemId = path + '/';
    }
    MenuFolder const *folder = m_rootFolder->findFolder(folderPath);
    if (!folder) {
        // parent folder not found - cannot move
        return false;
    }
    MenuItem const *item = separator ? folder->findItem(itemId.toInt()) : folder->findItem(itemId);
    if (!item) {
        // item not found - cannot move
        return false;
    }
    if (row == -1 && column == -1) {
        qDebug() << "Dropped on parent";
        if (folderItem) {
            return moveFolder(indexOf(const_cast<MenuItem*>(item)), parent, row);
        }
        beginInsertRows(parent, rowCount(parent), rowCount(parent) +1);
    } else {
        qDebug() << "Not dropped on parent";
        // check whether it's a movement in the same folder
        if ((parent.isValid() && folder == static_cast<MenuFolder*>(parent.internalPointer())) ||
                (!parent.isValid() && folder == m_rootFolder)) {
            const int sourceIndex = folder->indexOf(const_cast<MenuItem*>(item));
            if (sourceIndex == row || sourceIndex == row - 1) {
                // prevent no-ops
                return false;
            }
            beginMoveRows(parent, sourceIndex, sourceIndex, parent, row);
            const_cast<MenuFolder*>(folder)->moveItem(sourceIndex, row);
            endMoveRows();
            return true;
        }
        if (folderItem) {
            return moveFolder(indexOf(const_cast<MenuItem*>(item)), parent, row);
        }
        beginInsertRows(parent, row+1, row+2);
    }
    if (parent.isValid()) {
        static_cast<MenuFolder*>(parent.internalPointer())->insertItem(item, row);
    } else {
        m_rootFolder->insertItem(item, row);
    }
    endInsertRows();
    if (action == Qt::MoveAction) {
        QModelIndex oldIndex = indexOf(const_cast<MenuItem*>(item));
        setData(oldIndex, true, HiddenRole);
        const_cast<MenuItem*>(item)->setRemoved(true);
    }
    return true;
}

bool MenuModel::moveFolder(const QModelIndex& sourceIndex, const QModelIndex& targetParent, int row)
{
    if (!sourceIndex.data(FolderRole).toBool()) {
        return false;
    }
    if (row == -1) {
        row = rowCount(targetParent);
    }
    beginMoveRows(parent(sourceIndex), sourceIndex.row(), sourceIndex.row(), targetParent, row);
    static_cast<MenuFolder*>(sourceIndex.internalPointer())->moveToFolder(targetParent.isValid() ? static_cast<MenuFolder*>(targetParent.internalPointer()) : m_rootFolder, row);
    endMoveRows();
    return true;
}

Qt::DropActions MenuModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

void MenuModel::insertNewSeparator(const QModelIndex& index)
{
    if (!index.isValid()) {
        return;
    }
    const bool folder = static_cast<MenuItem*>(index.internalPointer())->isFolder();
    QModelIndex parent = folder  ? QModelIndex(index) : MenuModel::parent(index);
    if (folder) {
        beginInsertRows(parent, rowCount(parent), rowCount(parent) +1);
    } else {
        beginInsertRows(parent, index.row() + 1, index.row() + 1);
    }
    MenuFolder *menuFolder = parent.isValid() ? static_cast<MenuFolder*>(parent.internalPointer()) : m_rootFolder;
    menuFolder->insertNewSeparator(folder ? -1 : index.row() + 1);

    endInsertRows();
}

QModelIndex MenuModel::indexOf(MenuItem *item) const
{
    if (!item->parent()) {
        return QModelIndex();
    }
    if (item->parent() == m_rootFolder) {
        return index(m_rootFolder->indexOf(item), 0);
    }
    const QModelIndex parentIndex = indexOf(item->parent());
    return index(static_cast<MenuFolder*>(parentIndex.internalPointer())->indexOf(item), 0, parentIndex);
}

bool MenuModel::isChanged() const
{
    return m_rootFolder->isTreeChanged() || m_rootFolder->isLayoutDirty();
}

} // namespace
